from django.core.management.base import BaseCommand, CommandError
from dddns.models import Domain

class Command(BaseCommand):
    help = 'Get an invite code for a managed dynamic DNS domain'

    def add_arguments(self, parser):
        parser.add_argument('domain', nargs='+', type=str)

    def handle(self, *args, **options):
        for d in options['domain']:
            try:
                domain = Domain.objects.get(domain=d)
            except Domain.DoesNotExist:
                raise CommandError("Domain '%s' does not exist"%(d))
            i = domain.new_invite()
            self.stdout.write("invite code for %s: %s\n  (expires %s)"%(domain.domain, i.cleartext, i.expiration()))
