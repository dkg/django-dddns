from django.core.management.base import BaseCommand, CommandError
from dddns.models import *

class Command(BaseCommand):
    help = 'Verify that domains can be published (or all of them)'

    def add_arguments(self, parser):
        parser.add_argument('domain', nargs='*', type=str, help="domain(s) to check. (default: all domains)")

    def handle(self, *args, **options):
        if options['domain']:
            publishers = Publisher.objects.filter(domain__in=options['domain'])
        else:
            publishers = Publisher.objects.all()

        for p in publishers:
            p.check_connectivity()
        self.stdout.write("ok")
