from django.core.management.base import BaseCommand, CommandError
from django.db import connection

class Command(BaseCommand):
    help = 'Update database permissions for a constrained webapi'

    def add_arguments(self, parser):
        parser.add_argument('--webapi',
                            default="www-data",
                            help='The database role used by the webapi (default: www-data)')
        parser.add_argument('--publisher',
                            default="dddns_publisher",
                            help='The database role used by the publication daemon (default: dddns_publisher)')

    def handle(self, *args, **options):
        d = '''
GRANT SELECT ON django_migrations, dddns_publisher, dddns_staticrecord, dddns_domain TO "%(webapi)s", "%(publisher)s";
GRANT SELECT, UPDATE ON dddns_authorization_id_seq TO "%(webapi)s";
GRANT SELECT, INSERT ON dddns_authorization TO "%(webapi)s";
GRANT SELECT ON dddns_authorization TO "%(publisher)s";
GRANT SELECT, DELETE ON dddns_invite TO "%(webapi)s";
GRANT UPDATE (last_updated, serial) ON dddns_domain TO "%(webapi)s";
GRANT UPDATE (last_pushed, last_pushed_serial) ON dddns_publisher TO "%(publisher)s";
GRANT UPDATE ON dddns_authorization TO "%(webapi)s";
'''
        # FIXME: split out publisher permissions from webapi
        # FIXME: be more parsimonious on authorization (requires additional code changes):
        #    GRANT UPDATE (last_updated, ip, authcode) ON dddns_authorization TO "www-data";
        # this would replace the "GRANT UPDATE ON dddns_authorization"
        with connection.cursor() as cursor:
            cursor.execute(d%options)
