from django.core.management.base import BaseCommand, CommandError
from django.db import connection, connections
from dddns.models import *

class Command(BaseCommand):
    help = 'Publish the zone for some set of domains (or all of them)'

    def add_arguments(self, parser):
        parser.add_argument('--listen', action='store_true', help="run a foregrounded daemon that publishes when notified.")
        parser.add_argument('domain', nargs='*', type=str, help="domain(s) to publish. (default: all domains)")

    def handle(self, *args, **options):
        if options['listen']:
            self.run_listener(options['domain'])
        else:
            # do one-shot
            if options['domain']:
                domains = Domain.objects.filter(domain__in=options['domain'])
            else:
                domains = Domain.objects.all()
            for d in domains:
                d.publish(force=True)

    def run_listener(self, domains):
        import select
        import psycopg2
        import psycopg2.extensions
        # FIXME: accept a regular timeout interval at which point we'll publish the domains anyway?
        timeout=None

        # ensure that a connection exists, and report on domain monitoring:
        if domains:
            d = Domain.objects.filter(domain__in=domains).count()
            if d:
                self.stdout.write("monitoring %d %s for publication"%(d, 'domain' if d == 1 else 'domains'))
            else:
                raise CommandError("No domains found matching %s"%(domains))
        else:
            d = Domain.objects.count()
            self.stdout.write("monitoring all domains for publication (currently: %d %s)"%(d, 'domain' if d == 1 else 'domains'))
        
        connection.connection.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)

        with connection.connection.cursor() as cursor:
            cursor.execute("LISTEN dddns;")

            self.stdout.write("Waiting for notifications on channel 'dddns'")
            while True:
                if select.select([connection.connection],[],[],timeout) == ([],[],[]):
                    print("Timeout")
                else:
                    connection.connection.poll()
                    while connection.connection.notifies:
                        notify = connection.connection.notifies.pop(0)
                        if (not domains) or notify.payload in domains:
                            self.stdout.write("publishing %s"%(notify.payload))
                            Domain.objects.get(domain=notify.payload).publish()
