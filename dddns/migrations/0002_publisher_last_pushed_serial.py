# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-09-28 15:10
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dddns', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='publisher',
            name='last_pushed_serial',
            field=models.IntegerField(blank=True, null=True),
        ),
    ]
