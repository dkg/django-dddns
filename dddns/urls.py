from django.conf.urls import url

from . import views

app_name = 'dddns'
urlpatterns = [
    url(r'^update$', views.update, name='update'),
    url(r'^info$', views.authinfo, name='authinfo'),
    url(r'^refresh$', views.refresh, name='refresh'),
    url(r'^status$', views.status, name='status'),
    url(r'^remove$', views.remove, name='remove'),
    url(r'^enroll$', views.enroll, name='enroll'),
    url(r'^$', views.status),
    url(r'^getip$', views.getip, name='getip'),
]
