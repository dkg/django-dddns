from django.db import models
from django.utils import timezone
from datetime import timedelta
from random import SystemRandom
from base64 import b64encode,b64decode
from ipaddress import ip_address
from django.core.validators import RegexValidator
import subprocess
from tempfile import NamedTemporaryFile
import codecs
import os
from hashlib import sha512
from django.db import connection

class Domain(models.Model):
    '''A domain prepared for authorized update'''
    domain = models.CharField(max_length=200, unique=True)
    last_updated = models.DateTimeField(default=timezone.now)
    serial = models.IntegerField(default=1)
    primary_nameserver = models.CharField(max_length=100)
    hostmaster_email = models.EmailField(default="hostmaster")
    soa_ttl = models.DurationField(default=timedelta(days=1))
    soa_refresh = models.DurationField(default=timedelta(hours=3))
    soa_retry = models.DurationField(default=timedelta(minutes=15))
    soa_expire = models.DurationField(default=timedelta(weeks=1))
    soa_minimum = models.DurationField(default=timedelta(minutes=5))
    # the default, in case a related Authorization has ttl=None
    dynamic_ttl = models.DurationField(default=timedelta(minutes=5))
    invite_duration = models.DurationField(default=timedelta(weeks=1))
    max_publication_lag = models.DurationField(default=timedelta(minutes=1))
    hidden = models.BooleanField(default=False)

    @classmethod
    def create(cls, domainname, nameservers=["a.ns","b.ns"]):
        domain = cls(domain=domainname, primary_nameserver=nameservers[0])
        domain.save()
        for ns in nameservers:
            domain.new_static("@ 1D IN NS %s"%(ns))
        return domain

    def increment(self):
        'custom command to update the serial and last_updated values.'
        with connection.cursor() as cursor:
            t = timezone.now()
            cursor.execute("UPDATE dddns_domain SET serial = serial+1, last_updated = %s WHERE id = %s",
                           [t, self.id])
            cursor.execute("select serial from dddns_domain WHERE id = %s", [self.id])
            row = cursor.fetchone()
            self.serial = int(row[0])
            self.last_updated = t
    
    def save(self, *args, **kwargs):
        'Always increment the serial number when explicitly saved'
        self.serial += 1
        self.last_updated = timezone.now()
        super(Domain, self).save(*args, **kwargs)
        
    def __str__(self):
        return '%s%s : %s'%(self.domain, ' (hidden)' if self.hidden else '', self.publication_status())

    def get_hostmaster(self):
        "returns the hostmaster e-mail in zone form"
        return self.strip_suffix(str(self.hostmaster_email).replace('@', '.'))

    def strip_suffix(self, s):
        suffix = self.domain + "."
        if (s.endswith(suffix)):
            return s[:len(s)-len(suffix)]
        else:
            return s

    def soa(self):
        return "@ %d IN SOA %s %s %d %d %d %d %d"%(
            int(self.soa_ttl.total_seconds()),
            self.strip_suffix(self.primary_nameserver),
            self.get_hostmaster(),
            self.serial,
            int(self.soa_refresh.total_seconds()),
            int(self.soa_retry.total_seconds()),
            int(self.soa_expire.total_seconds()),
            int(self.soa_minimum.total_seconds()))

    def static(self):
        return StaticRecord.objects.filter(domain=self)

    def authorized(self):
        return Authorization.objects.filter(domain=self, ip__isnull=False)
    
    def zone_contents(self):
        return self.soa() + '\n' + \
            '\n'.join(map(str, self.static())) + \
            '\n; dynamic records follow\n' + \
            '\n'.join(map(Authorization.get_rr, self.authorized())) + \
            '\n; dddns -- end of ' + self.domain + '\n'

    def new_invite(self):
        i = Invite(domain=self)
        i.update_code()
        i.save()
        return i

    def new_publisher(self, host, user=None):
        p = Publisher(domain=self, host=host, user=user)
        p.save()
        return p

    def new_static(self, record):
        s = StaticRecord(domain=self,entry=record)
        s.save()
        return s

    def validate_zone(self):
        'This function needs kzonecheck (from the "knot" package)'
        f = NamedTemporaryFile(delete=False)
        f.write(codecs.encode(self.zone_contents()))
        fname = f.name
        f.close()
        p = subprocess.Popen(['kzonecheck',
                              '-o', self.domain,
                              fname],
                             shell=False)
        (sout, serr) = p.communicate(input=None)
        retcode = p.wait()
        os.unlink(fname)
        if serr:
            raise Exception(serr)
        if retcode:
            raise Exception("retcode: %d"%(retcode))

    def publication_status(self):
        should = timezone.now() - self.max_publication_lag
        active = Publisher.objects.filter(domain=self, active=True)
        activecount = active.count()
        pub = 'publisher' if activecount == 1 else 'publishers'

        ok = active.filter(last_pushed_serial=self.serial).count()
        if (ok == activecount):
            return 'ok [%d %s]'%(activecount, pub)
        pending = active.exclude(last_pushed_serial=self.serial).filter(last_pushed__gte=should).count()
        if pending < activecount - ok:
            return 'bad [%d bad, %d pending of %d %s]'%(
                activecount - (pending + ok), pending, activecount, pub)
        else:
            return 'pending [%d pending of %d %s]'%(pending, activecount, pub)
        
    def notify_publish(self):
        with connection.cursor() as cursor:
            cursor.execute("NOTIFY dddns, %s", [self.domain])

    def publish(self, force=False):
        '''
        publish zone to all publishers
        
        if force=True, publish even to publishers which seem to be already up-to-date
        '''
        publishers = Publisher.objects.filter(domain=self, active=True)
        if not force:
            publishers = publishers.filter(last_pushed_serial__lt=self.serial)
        for p in publishers:
            p.publish()


class StaticRecord(models.Model):
    '''an arbitary static RR in a given zone

FIXME: this is just the raw line -- we could be more sophisticated for
example, if there are A records here, we want to make sure they don't
get clobbered by an Authorization

OTOH, this is the most flexible way that we can manually add entries
to the zone, like apex nameservers.

    '''
    entry = models.CharField(max_length=1024)
    domain = models.ForeignKey(Domain, on_delete=models.PROTECT)

    def save(self, *args, **kwargs):
        'Always let the domain know that it has been updated'
        self.domain.increment()
        super(StaticRecord, self).save(*args, **kwargs)

    def __str__(self):
        return self.entry

class Authcode:
    '''High-entropy Authorization codes

    We generate high-entropy authorization codes as UTF-8-encoded
    strings, and store their hashes as hexadecimal strings.

    It would be simpler to store and transmit everything as binary
    objects instead of strings, but: we need strings to pass to the
    user, and we wouldn't be able to search for binary objects in the
    database.
    
    The choice of hash function is not particularly expensive to
    compute individually, but we rely on the fact that the codes
    themselves are high-enough entropy in the first place to make it
    very expensive for anyone with access to the underlying databaes
    to learn any authorization code or invite.

    If, in the future, we want to switch digests to something other
    than SHA512, we should add a new field to the Authorization and
    Invite models that stores the new digest.

    '''
    def __init__(self, size):
        self.size = size

    def sha512hash(self, s):
        'Return a SHA512 hash of the underlying data'
        b = b64decode(codecs.encode(s), altchars=b'-_')
        if len(b) != self.size:
            raise Exception("wrong size authentication code")
        return sha512(b).hexdigest()

    def generate(self):
        return codecs.decode(b64encode(
            SystemRandom().getrandbits(self.size*8).to_bytes(self.size, byteorder='big'),
            altchars=b'-_' # URL-safety RFC 7515
        ))
    
class Authorization(models.Model):
    '''an authorization (and its current value) for an address record

A NULL value for authcode means that the record is locked and cannot
be updated.

FIXME: add constraint that only one unique name per domain is allowed
       ?  but what about hosts that want to update both A and AAAA
       records?  perhaps this logic is enforced in the view that
       grants a new Authorization instead, or just by
       Invite.activate() below?

    '''
    name = models.CharField(max_length=100,
                            validators=[RegexValidator('^[a-z0-9-_]*$')])
    domain = models.ForeignKey(Domain, on_delete=models.PROTECT)
    authcode = models.CharField(max_length=128,
                                unique=True,
                                null=True, blank=True)
    ip = models.GenericIPAddressField(null=True, blank=True)
    last_updated = models.DateTimeField(default=timezone.now)
    ttl = models.DurationField(null=True, blank=True)
    cleartext = None
    codegen = Authcode(18)

    @classmethod
    def find(cls, code):
        return cls.objects.get(authcode=cls.codegen.sha512hash(code))

    def save(self, *args, **kwargs):
        'Always let the domain know that it has been updated'
        self.domain.increment()
        super(Authorization, self).save(*args, **kwargs)

    def update_code(self):
        self.cleartext = self.codegen.generate()
        self.authcode = self.codegen.sha512hash(self.cleartext)

    def get_ttl(self):
        if (self.ttl):
            return self.ttl
        else:
            return self.domain.dynamic_ttl

    def addr_family(self):
        if (self.ip is None):
            return None
        addr = ip_address(self.ip)
        if addr.version == 4:
            return 'A'
        elif addr.version == 6:
            return 'AAAA'
        else:
            raise "unknown IP address version %d"%(addr.version)

    def __str__(self):
        return "%s.%s%s%s"%(
            self.name,
            self.domain.domain,
            ((": " + self.cleartext) if self.cleartext else ""),
            ((": " + self.get_rr()) if self.ip else ""))
        
    def get_rr(self):
        if (self.ip is None):
            return None
        return "%s %d IN %s %s"%(
            self.name,
            int(self.get_ttl().total_seconds()),
            self.addr_family(),
            self.ip)


class Invite(models.Model):
    'An invitation for a specific domain'
    domain = models.ForeignKey(Domain, on_delete=models.PROTECT)
    invite_code = models.CharField(max_length=128,
                                   unique=True)
    created = models.DateTimeField(default=timezone.now)
    cleartext = None
    codegen = Authcode(12)

    @classmethod
    def find(cls, code):
        return cls.objects.get(invite_code=cls.codegen.sha512hash(code))

    def __str__(self):
        return "domain: %s%s (created: %s, expires: %s)"%(
            self.domain.domain,
            (" invite code: " + self.cleartext if self.cleartext else ""),
            self.created,
            self.created + self.domain.invite_duration)

    def expiration(self):
        return self.created + self.domain.invite_duration

    def is_expired(self):
        if self.id is None:
            return True
        return (timezone.now() > self.created + self.domain.invite_duration)

    def update_code(self):
        self.cleartext = self.codegen.generate()
        self.invite_code = self.codegen.sha512hash(self.cleartext)
    
    def activate(self, name):
        'activate the invitation, destroying the Invite if successful, or raising an exception otherwise'
        if self.is_expired():
            raise Exception("Invite Code expired, sorry")
        if Authorization.objects.filter(domain=self.domain, name=name).count() > 0:
            raise Exception("name %s.%s already taken"%(name, self.domain.domain))
        a = Authorization(domain=self.domain, name=name)
        a.update_code()
        a.save()
        self.delete()
        return a


class Publisher(models.Model):
    '''sending data for the zone to a remote nameserver via ssh

    FIXME: this pushes the entire zone on every change, which costs a
    lot of network traffic for large zones with frequently-changing
    records.  We could be much more efficient in terms of network
    communication if we just used native psql replication and
    triggered a local reload of the file on each change.

    '''
    domain = models.ForeignKey(Domain, on_delete=models.PROTECT)
    host = models.CharField(max_length=256)
    user = models.CharField(max_length=256, null=True, blank=True)
    active = models.BooleanField(default=True)
    last_pushed = models.DateTimeField(null=True, blank=True)
    last_pushed_serial = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return "publish zone '%s' to %s [%s]"%(
            self.domain.domain, self.remote_name(), self.publication_status())

    def remote_name(self):
        if self.user:
            return "%s@%s"%(self.user, self.host)
        return self.host

    def successfully_pushed(self, serial):
        'narrow database update to indicate that a publisher successfully pushed'
        with connection.cursor() as cursor:
            t = timezone.now()
            cursor.execute("UPDATE dddns_publisher SET last_pushed_serial = %s, last_pushed = %s WHERE id = %s",
                           [serial, t, self.id])
            self.last_pushed_serial = serial
            self.last_pushed = t

    def get_ssh_args(self):
        args = ['ssh']
        if self.user:
            args += ['-l', self.user]
        args += [self.host]
        return args

    def check_connectivity(self):
        'test whether the ssh connections work as expected'
        args = self.get_ssh_args()
        args += ['check']
        p = subprocess.Popen(args,
                             universal_newlines=True,
                             stdin=subprocess.PIPE,
                             stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE,
                             shell=False)
        (sout, serr) = p.communicate(input=None)
        if serr:
            raise Exception("Check of '%s' to %s returned error %s"%(
                self.domain.domain,
                self.remote_name(),
                serr))
        if sout.strip() != 'ok':
            raise Execption("Check of '%s' to %s did not produce 'OK'"%(
                self.domain.domain,
                self.remote_name()))
        retcode = p.wait()
        if retcode:
            raise Exception("during check of '%s' to %s, retcode: %d"%(
                self.domain.domain,
                self.remote_name(),
                retcode))

    def publication_status(self):
        'returns inactive, ok, pending, bad depending on whether the latest zone is late'
        if not self.active:
            return 'inactive'
        if self.last_pushed_serial is not None and (self.last_pushed_serial == self.domain.serial):
            return 'ok'
        if self.last_pushed_serial is not None and self.last_pushed_serial > self.domain.serial:
            # FIXME: this should probably be a database constraint
            raise Exception("zone serial is less than publisher serial")
        if timezone.now() < self.domain.last_updated + self.domain.max_publication_lag:
            return 'pending'
        return 'bad'

        
    def publish(self):
        'use ssh to push the zone info out'
        args = self.get_ssh_args()
        d = self.domain
        args += ["update", d.domain]
        p = subprocess.Popen(args,
                             universal_newlines=True,
                             stdin=subprocess.PIPE,
                             stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE,
                             shell=False)
        (sout, serr) = p.communicate(input=d.zone_contents())
        if serr:
            raise Exception("Push of '%s' to %s returned error %s"%(
                self.domain,
                self.remote_name(),
                serr))
        retcode = p.wait()
        if retcode:
            raise Exception("retcode: %d"%(retcode))

        self.successfully_pushed(d.serial)
