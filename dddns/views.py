from dddns.models import Authorization, Invite, Domain
from django.http import HttpResponse
from django.http import Http404, HttpResponseBadRequest
from django.db.utils import DataError
from django.views.decorators.http import require_POST
from django.template import loader
from django.shortcuts import redirect
from ipaddress import ip_address

def ips_same(a,b):
    if a is None:
        return b is None
    if b is None:
        return False
    return str(ip_address(a)) == str(ip_address(b))


@require_POST
def update(request):
    '''update an existing authorization, based on auth code and IP address.

    '''
    try:
        authcode = request.POST['authcode']
        a = Authorization.find(authcode)
    except:
        raise Http404("Authentication code does not exist")
    ip = request.POST.get('ip', None)
    if ip:
        ip = ip.strip()
    if not ip:
        try:
            ip = request.META['REMOTE_ADDR']
        except:
            return HttpResponseBadRequest("Must supply an IP address")

    try:
        if ips_same(ip,a.ip):
            return HttpResponse('success\n', content_type="text/plain")
    except ValueError:
        return HttpResponseBadRequest("IP address is likely wrong")
    a.ip = ip
    try:
        a.save()
    except DataError:
        return HttpResponseBadRequest("IP address is likely wrong")
    a.domain.notify_publish()
    return HttpResponse('success\n', content_type="text/plain")


@require_POST
def remove(request):
    '''remove IP address from existing authorization, based on auth code.
    '''
    authcode = request.POST['authcode']
    a = Authorization.find(authcode)
    if a.ip is None:
        return HttpResponse('success\n', content_type="text/plain")
    a.ip = None
    a.save()
    a.domain.notify_publish()
    return HttpResponse('success\n', content_type="text/plain")


def enroll(request):
    '''enroll the user for a given domain, based on an invite code'''
    if request.method == 'POST':
        try:
            invite = request.POST['invite']
            i = Invite.find(invite)
        except:
            raise Http404("Invite code does not exist")
        if i.is_expired():
            return HttpResponseNotAllowed("Invite code has expired")
        name = request.POST.get('name', None)
        if name:
            auth = i.activate(request.POST['name'])
            template = loader.get_template('auth.html')
            return HttpResponse(template.render({'auth': auth }, request))
        else:
            i.cleartext = invite
            template = loader.get_template('enroll.html')
            return HttpResponse(template.render({'invite': i}, request))
    else:
        template = loader.get_template('enroll-empty.html')
        return HttpResponse(template.render({}, request))


def authinfo(request):
    if request.method == 'POST':
        try:
            authcode = request.POST['authcode']
            auth = Authorization.find(authcode)
        except:
            raise Http404("Authentication code does not exist")
        auth.cleartext = authcode
        template = loader.get_template('auth.html')
        return HttpResponse(template.render({'auth': auth }, request))
    else:
        template = loader.get_template('auth-empty.html')
        return HttpResponse(template.render({}, request))

def getip(request):
    ip = request.META['REMOTE_ADDR']
    return HttpResponse(ip, content_type="text/plain")

def refresh(request):
    if request.method == 'POST':
        try:
            authcode = request.POST['authcode']
            a = Authorization.find(authcode)
        except:
            raise Http404("Invite code does not exist")

        ok = request.POST.get("ok", False)
        if not ok:
            template = loader.get_template('refresh.html')
            a.cleartext = authcode
            return HttpResponse(template.render({'auth': a}, request))

        a.update_code()
        a.save()
        template = loader.get_template('auth.html')
        return HttpResponse(template.render({'auth': a}, request))
    else:
        template = loader.get_template('refresh-empty.html')
        return HttpResponse(template.render({}, request))
            
def status(request):
    template = loader.get_template('status.html')
    return HttpResponse(template.render({'domains': Domain.objects.filter(hidden=False)}, request))
