Things to do for django-dddns
=============================

Replicated DBs -- the current approach pushes the whole zone over
ssh every time any record is updated.  It would probably take less
bandwidth to have a "hot standby" replicated postgresql database on
each authoritative resolver, and have them rewrite the zone file
directly.

When adding a domain initially, it'd be nice to be clever about the
nameservers passed in -- maybe we could auto-create glue records, for
example.

make touching "dddns_authorization.{last_updated,ip,authcode}"
narrowly-targeted SQL commands.  This would let us constrain www-data
even further.  see
file:///usr/share/doc/python-django-doc/html/topics/db/sql.html#executing-custom-sql-directly

Make webapis and publication step use database transactions, so that
we have atomic zone updates.

publication daemon probably should parallelize subprocesses -- we
don't want one ssh update to cause the other ones to hang, either
within a domain or across domains.

publication daemon might want to sign the zone with DNSSEC before
pushing.

publication daemon will choke and die if an ssh service isn't
available.  it should be more robust, and continue publishing to those
services which remain available.

change systemd .service file to a template, so that you can
instantiate publishers explicitly without editing the .service file.

publication daemon should try to publish via every pending or bad zone
immediately at startup, before starting to listen.

Cleanup
-------

write test suite

look into packaging/distribution to make it easier to deploy

visual styling for those interfaces that humans might look at.

HTML standards compliance

deletion of expired/unused invitation codes

Clearer error message when the user tries to register an
already-claimed name in the invited zone.

Policy questions (maybe these are per-zone policies?)
-----------------------------------------------------

destroying auth codes entirely from the webapp, as opposed to just
removing the IP address from them (should it be disabled from
re-registration for some period of time?)

track number of updates of an auth code? (either IP or changes to the
authcode string itself)
